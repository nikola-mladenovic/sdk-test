//
//  CommercetoolsSDK.h
//  CommercetoolsSDK
//
//  Created by Nikola Mladenovic on 4/13/16.
//  Copyright © 2016 Commercetools. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CommercetoolsSDK.
FOUNDATION_EXPORT double CommercetoolsSDKVersionNumber;

//! Project version string for CommercetoolsSDK.
FOUNDATION_EXPORT const unsigned char CommercetoolsSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommercetoolsSDK/PublicHeader.h>


